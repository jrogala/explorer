package com.example.rgl95.explorer.fragments.browseMaps.di;

import com.example.rgl95.explorer.fragments.browseMaps.BrowseMapsContract;
import com.example.rgl95.explorer.fragments.browseMaps.BrowseMapsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class BrowseMapsModule {

    private BrowseMapsContract.View view;

    public BrowseMapsModule(BrowseMapsContract.View view) {
        this.view = view;
    }

    @Provides
    BrowseMapsContract.Presenter providePresenter() {
        return new BrowseMapsPresenter(view);
    }
}
