package com.example.rgl95.explorer.fragments.browseMaps.di;

import com.example.rgl95.explorer.fragments.browseMaps.BrowseMapsFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {BrowseMapsModule.class})
public interface BrowseMapsComponent {

    void inject(BrowseMapsFragment fragment);
}
