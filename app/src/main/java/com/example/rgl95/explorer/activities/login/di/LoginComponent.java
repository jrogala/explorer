package com.example.rgl95.explorer.activities.login.di;

import com.example.rgl95.explorer.activities.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {LoginModule.class})
public interface LoginComponent {

    void inject(LoginActivity loginActivity);
}
