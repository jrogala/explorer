package com.example.rgl95.explorer.activities.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.rgl95.explorer.Explorer;
import com.example.rgl95.explorer.R;
import com.example.rgl95.explorer.activities.main.di.MainModule;
import com.example.rgl95.explorer.fragments.browseMaps.BrowseMapsFragment;
import com.example.rgl95.explorer.fragments.map.MapFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.drawer_toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.navigation_view)
    NavigationView naviView;

    @Inject
    MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setToolbar();
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        ((Explorer) getApplication()).getAppComponent().plus(new MainModule(this)).inject(this);

        naviView.setNavigationItemSelectedListener(menuItem -> {

            drawerLayout.closeDrawers();
            showFragment(menuItem);

            return true;
        });
    }

    private void showFragment(MenuItem menuItem) {

        Fragment chosenFragment = null;


        switch (menuItem.getItemId()) {
            case R.id.menu_browse_maps:
                chosenFragment = new BrowseMapsFragment();
                break;

            case R.id.menu_home:
                chosenFragment = new MapFragment();
                break;

        }
        if (chosenFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.drawer_layout_container, chosenFragment)
                    .commit();
        }


    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }
        getWindow().setStatusBarColor(Color.TRANSPARENT);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(Gravity.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START))
            drawerLayout.closeDrawers();
        else
            super.onBackPressed();
    }


}