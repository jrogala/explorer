package com.example.rgl95.explorer.activities.login;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.rgl95.explorer.Explorer;
import com.example.rgl95.explorer.R;
import com.example.rgl95.explorer.activities.login.di.LoginModule;
import com.example.rgl95.explorer.activities.main.MainActivity;
import com.example.rgl95.explorer.data.User;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.rgl95.explorer.Constant.PERMISSION_CODE;
import static com.example.rgl95.explorer.Constant.SETTINGS_CODE;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @BindView(R.id.register_email)
    EditText registerLoginET;
    @BindView(R.id.register_password)
    EditText registerPasswordET;
    @BindView(R.id.register_password_confirmation)
    EditText registerPasswordConfirmationET;

    @BindView(R.id.login_email)
    EditText loginET;
    @BindView(R.id.login_password)
    EditText loginPasswordET;

    @Inject
    LoginContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        ((Explorer) getApplication()).getAppComponent().plus(new LoginModule(this)).inject(this);
    }

    @OnClick(R.id.login_button)
    public void signIn() {
        presenter.signIn(
                loginET.getText().toString(),
                loginPasswordET.getText().toString()
        );
    }

    @OnClick(R.id.register_button)
    public void signUp() {

        User user = new User(
                registerLoginET.getText().toString(),
                registerPasswordET.getText().toString(),
                registerPasswordConfirmationET.getText().toString()
        );
//        presenter.signUpUser(user);
        isGPSEnabled();
    }

    private void isGPSEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!(manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER))) {
            buildDialogNoGPS();
        } else
            startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.checkPermissions();
    }

    @Override
    public void getPermissions() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE);
    }

    private void buildDialogNoGPS() {
        Dialog dialog = new AlertDialog.Builder(this)
                .setCancelable(true)
                .setPositiveButton("Go", (dialogInterface, i) -> {
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), SETTINGS_CODE);
                })
                .setMessage("GPS in disabled.")
                .create();

        dialog.show();
    }

}