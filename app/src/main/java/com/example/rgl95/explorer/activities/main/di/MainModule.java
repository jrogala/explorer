package com.example.rgl95.explorer.activities.main.di;

import com.example.rgl95.explorer.activities.main.MainContract;
import com.example.rgl95.explorer.activities.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    private MainContract.View view;

    public MainModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    MainContract.Presenter providesPresenter() {
        return new MainPresenter(view);
    }
}
