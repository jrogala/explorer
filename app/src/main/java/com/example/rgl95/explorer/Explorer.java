package com.example.rgl95.explorer;

import android.app.Application;

import com.example.rgl95.explorer.di.AppComponent;
import com.example.rgl95.explorer.di.AppModule;
import com.example.rgl95.explorer.di.DaggerAppComponent;

public class Explorer extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
