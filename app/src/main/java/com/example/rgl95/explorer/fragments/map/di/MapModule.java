package com.example.rgl95.explorer.fragments.map.di;

import com.example.rgl95.explorer.fragments.map.MapContract;
import com.example.rgl95.explorer.fragments.map.MapPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MapModule {

    private MapContract.View view;

    public MapModule(MapContract.View view) {
        this.view = view;
    }

    @Provides
    MapContract.Presenter providesPresenter() {
        return new MapPresenter(view);
    }
}