package com.example.rgl95.explorer;

import com.example.rgl95.explorer.data.browseAllMaps.MapResponse;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    String BASE_URL = " https://explo-api.herokuapp.com/api/v1/";


    @FormUrlEncoded
    @POST("sign_up")
    Call<ResponseBody> signUp(
            @Field("user[email]") String email,
            @Field("user[password]") String password,
            @Field("user[password_confirmation]") String passwordConfirmation
    );

    @FormUrlEncoded
    @POST("sign_in")
    Call<ResponseBody> signIn(
            @Field("sign_in[email]") String email,
            @Field("sign_in[password]") String password
    );

    @GET("maps")
    Single<MapResponse> getAllMaps();

}
