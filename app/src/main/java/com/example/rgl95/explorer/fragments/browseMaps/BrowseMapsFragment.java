package com.example.rgl95.explorer.fragments.browseMaps;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rgl95.explorer.Explorer;
import com.example.rgl95.explorer.R;
import com.example.rgl95.explorer.data.browseAllMaps.Map;
import com.example.rgl95.explorer.fragments.browseMaps.di.BrowseMapsModule;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrowseMapsFragment extends Fragment implements BrowseMapsContract.View {

    @BindView(R.id.browse_maps_recycler)
    RecyclerView recyclerView;

    @Inject
    BrowseMapsContract.Presenter presenter;
    BrowseMapsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_browse_all_maps, container, false);
        ButterKnife.bind(this, view);

        ((Explorer) Objects.requireNonNull(getActivity()).getApplication()).getAppComponent().plus(new BrowseMapsModule(this)).inject(this);

        adapter = new BrowseMapsAdapter();

        presenter.getAllMaps();

        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setAdapter(adapter);
        return view;
    }


    @Override
    public void updateData(List<Map> maps) {
        adapter.updateList(maps);
    }
}
