package com.example.rgl95.explorer.activities.main.di;

import com.example.rgl95.explorer.activities.main.MainActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {MainModule.class})
public interface MainComponent {

    void inject(MainActivity mainActivity);
}
