package com.example.rgl95.explorer;

public interface Constant {
    int PERMISSION_CODE = 1000;
    int SETTINGS_CODE = 1001;
    String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

}
