package com.example.rgl95.explorer.data;

public class User {

    private String email;
    private String password;
    private String password_confirmation;

    public User(String email, String password, String password_confirmation) {
        this.email = email;
        this.password = password;
        this.password_confirmation = password_confirmation;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

}
