package com.example.rgl95.explorer.fragments.browseMaps;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rgl95.explorer.R;
import com.example.rgl95.explorer.data.browseAllMaps.Map;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrowseMapsAdapter extends RecyclerView.Adapter<BrowseMapsAdapter.ViewHolder> {

    private List<Map> mapList = new ArrayList<>();

    void updateList(List<Map> maps) {
        mapList.clear();
        mapList = maps;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return new     ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_map, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setupMap(mapList.get(i));
    }

    @Override
    public int getItemCount() {
        return mapList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.map_name)
        TextView mapName;

        @BindView(R.id.map_description)
        TextView mapDescription;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setupMap(Map map) {
            mapName.setText(map.getName());
            mapDescription.setText(map.getDescription());
        }
    }
}
