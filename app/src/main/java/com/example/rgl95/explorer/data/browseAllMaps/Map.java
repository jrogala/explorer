package com.example.rgl95.explorer.data.browseAllMaps;

public class Map {

    private int id;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}