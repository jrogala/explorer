package com.example.rgl95.explorer.data.browseAllMaps;

import com.google.gson.annotations.SerializedName;


public class MapResponse {

    @SerializedName("is_success")
    private boolean isSuccess;

    private Data data;

    public boolean isSuccess() {
        return isSuccess;
    }

    public Data getData() {
        return data;
    }
}
