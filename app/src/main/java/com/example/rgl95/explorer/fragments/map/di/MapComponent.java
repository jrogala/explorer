package com.example.rgl95.explorer.fragments.map.di;


import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {MapModule.class})
public interface MapComponent {
}
