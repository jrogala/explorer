package com.example.rgl95.explorer.di;

import com.example.rgl95.explorer.ApplicationScope;
import com.example.rgl95.explorer.activities.login.di.LoginComponent;
import com.example.rgl95.explorer.activities.login.di.LoginModule;
import com.example.rgl95.explorer.activities.main.di.MainComponent;
import com.example.rgl95.explorer.activities.main.di.MainModule;
import com.example.rgl95.explorer.fragments.browseMaps.di.BrowseMapsComponent;
import com.example.rgl95.explorer.fragments.browseMaps.di.BrowseMapsModule;
import com.example.rgl95.explorer.fragments.map.di.MapComponent;
import com.example.rgl95.explorer.fragments.map.di.MapModule;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    LoginComponent plus(LoginModule loginModule);

    MainComponent plus(MainModule mainModule);

    MapComponent plus(MapModule mapModule);

    BrowseMapsComponent plus(BrowseMapsModule browseMapsModule);

}
