package com.example.rgl95.explorer.di;

import android.content.Context;

import com.example.rgl95.explorer.ApiService;
import com.example.rgl95.explorer.ApplicationScope;
import com.example.rgl95.explorer.data.RetrofitClient;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    Context providesContext() {
        return context;
    }

    @Provides
    @ApplicationScope
    ApiService providesApi() {
        return RetrofitClient.getInstance().getApi();
    }


}
