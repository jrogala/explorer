package com.example.rgl95.explorer.activities.login;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.example.rgl95.explorer.ApiService;
import com.example.rgl95.explorer.data.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;
    private ApiService api;

    public LoginPresenter(LoginContract.View view, ApiService apiService) {
        this.view = view;
        this.api = apiService;
    }

    @Override
    public void signUpUser(User user) {
        Call<ResponseBody> signUpCall = api.signUp(user.getEmail(), user.getPassword(), user.getPassword());

        signUpCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                Toast.makeText((Context) view, response.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                Toast.makeText((Context) view, "lipa", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void signIn(String email, String password) {
        Call<ResponseBody> signInCall = api.signIn(email, password);

        signInCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Toast.makeText((Context) view, response.toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText((Context) view, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void checkPermissions() {
        if (!(ContextCompat.checkSelfPermission(
                ((Context) view).getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            view.getPermissions();
        }

        if (!(ContextCompat.checkSelfPermission(
                ((Context) view).getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            view.getPermissions();

        }

    }
}
