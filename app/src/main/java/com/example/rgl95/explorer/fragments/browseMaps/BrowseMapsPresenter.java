package com.example.rgl95.explorer.fragments.browseMaps;

import com.example.rgl95.explorer.ApiService;
import com.example.rgl95.explorer.data.RetrofitClient;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BrowseMapsPresenter implements BrowseMapsContract.Presenter {

    private BrowseMapsContract.View view;
    private ApiService api;
    private CompositeDisposable disposable;

    public BrowseMapsPresenter(BrowseMapsContract.View view) {
        this.view = view;

        api = RetrofitClient
                .getInstance()
                .getApi();

        disposable = new CompositeDisposable();
    }


    @Override
    public void getAllMaps() {
        disposable.add(api.getAllMaps()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mapResponse -> view.updateData(mapResponse.getData().getMaps())));

    }
}
