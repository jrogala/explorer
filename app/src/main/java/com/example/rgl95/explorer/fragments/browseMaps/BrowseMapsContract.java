package com.example.rgl95.explorer.fragments.browseMaps;


import com.example.rgl95.explorer.data.browseAllMaps.Map;

import java.util.List;

public interface BrowseMapsContract {

    interface View {

        void updateData(List<Map> maps);
    }

    interface Presenter {

        void getAllMaps();
    }

}
