package com.example.rgl95.explorer.activities.login;

import com.example.rgl95.explorer.data.User;

public interface LoginContract {

    interface View {

        void getPermissions();
    }

    interface Presenter {

        void signUpUser(User user);

        void signIn(String s, String s1);

        void checkPermissions();
    }
}
