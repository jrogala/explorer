package com.example.rgl95.explorer.activities.login.di;


import com.example.rgl95.explorer.ApiService;
import com.example.rgl95.explorer.activities.login.LoginContract;
import com.example.rgl95.explorer.activities.login.LoginPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    private LoginContract.View view;

    public LoginModule(LoginContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    LoginContract.Presenter providesPresenter(ApiService apiService) {
        return new LoginPresenter(view, apiService);
    }
}
